package level1;

public class Service {
    int[] ar;
    int a;

    public Service(int[] ar, int a) {
        this.ar = ar;
        this.a = a;
    }
    public static void printArray(int[] ar) {
        System.out.println("Все значения в массиве");
        for (int l : ar) {
            System.out.print(l + " ");
        }
    }
    public static int modul (int a)
    {
        if (a<0) a=a*-1;
        return(a);
    }
}
