package level1;

public class Tasks {
    public void Task1 (int a, int b) {
        System.out.println(((a - b) < 0) ? a : b);
    }

    public void Task2 (int[] arr, int K) {
        Service.printArray(arr);
        int sum=0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % K == 0) {
                sum = arr[i] + sum;
            }
        }
        System.out.print("Сумма всех значений кратных "+K+" = ");
        System.out.println(sum);
    }

    public void Task3(int[]arr){
        int ar3 =0;
        for (int i=0; i<arr.length; i++){
            if(arr[i]==0){
                ar3++;
            }
        }
        int[] arr31 =new int[ar3];
        int n=0;
        for (int i=0; i<arr.length; i++){
            if(arr[i]==0){
                arr31[n]=i;
                n++;
            }
        }
        Service.printArray(arr);
        System.out.println();
        Service.printArray(arr31);
        System.out.println();
    }

    public void Task4(int[]arr) {
        Service.printArray(arr);
        System.out.println();
        for (int i=0; i<arr.length; i++){
            if(arr[i]<0){
                System.out.println("Первое отрицательное"); break;
            }else{ if(arr[i]>0){System.out.println("Первое положительное"); break;
            }
            }
        }

    }

    public void Task5(int[]arr) {
        String out = "Отсортирован";
        Service.printArray(arr);
        System.out.println();
        for (int i=0; i<(arr.length-1); i++){
            if(arr[i]>arr[i+1]) {
                out = "Не отсортирован";
                break;
            }
        }
        System.out.println(out);

    }

    public void Task6(int[]arr) {
        int ar6 =0;
        for (int i=0; i<arr.length; i++){
            if((arr[i]%2)==0){
                ar6++;
            }
        }
        int[] arr61 =new int[ar6];
        int n1=0;
        for (int i=0; i<arr.length; i++){
            if(arr[i]%2==0){
                arr61[n1]=arr[i];
                n1++;
            }
        }
        Service.printArray(arr);
        System.out.println();
        Service.printArray(arr61);

    }

    public void Task7(int Z, int[]arr) {
        int count =0;
        Service.printArray(arr);
        for (int i=0; i<arr.length; i++){
            if(arr[i]>Z){
                arr[i]=Z;
                count++;
            }
        }
        Service.printArray(arr);
        System.out.println();
        System.out.println("Z = "+Z);
        System.out.println("Количество замен = "+count);

    }

    public void Task8(int[]arr) {
        Service.printArray(arr);
        int count0 =0;
        int minus = 0;
        int plus = 0;
        for (int k:arr){
            if (k<0) minus++;
            else if(k>0)plus++;
            else count0++;
        }
        System.out.println();
        System.out.println("Положительных - "+plus);
        System.out.println("Отрицательных - "+minus);
        System.out.println("Нулевых - "+count0);


    }

    public void Task9(int[]arr) {
        int[] arr9t= new int[5];
        //arr9t[0] максимальное значение
        //arr9t[1] номер ячейки максимального значения
        //arr9t[2] минимальное значение
        //arr9t[3] номер ячейки минимального значения
        for (int i=0; i<arr.length; i++){
            if(arr[i]>arr9t[0]) {
                arr9t[0]=arr[i];
                arr9t[1]=i;
            }else{
                if(arr[i]<arr9t[2]) {
                    arr9t[2]=arr[i];
                    arr9t[3]=i;
                }
            }
        }
        Service.printArray(arr);
        arr[arr9t[1]]=arr9t[2];
        arr[arr9t[3]]=arr9t[0];
        System.out.println();
        Service.printArray(arr);

    }

    public void Task10(int[]arr) {
        Service.printArray(arr);
        System.out.println();
        for (int i=0; i<arr.length;i++) {
            if(arr[i]<i)
                System.out.print(arr[i] + " ");
        }
    }

    public void Task11(int[] arr, int M, int L) {
        Service.printArray(arr);
        System.out.println();
        System.out.println("M = "+M);
        System.out.println("L = "+L);
        for (int k:arr) {
            if(k%M==L)
                System.out.print(k + " ");
        }


    }

    public void Task12(int[]arr) {
        Service.printArray(arr);
        try {
            int temp;
            for(int i=0; i<arr.length; i+=2){
                temp=arr[i]; arr[i]=arr[(i+1)]; arr[(i+1)]=temp;
                //i++;
            }
        }catch (Exception oor){
            System.out.println();
            System.out.println("Не чётная длина массива!");
        }
        Service.printArray(arr);
    }

    public void Task13(int[]arr) {
        Service.printArray(arr);
        int count13=0;
        for(int i=0; i<arr.length; i++){
            if (arr[i]==0){
                count13 = i+1; break;
            }
        }
        int [] arr131 = new int[count13];
        for (int i=0; i<arr131.length; i++){
            arr131[i]=arr[i];
        }
        System.out.println();
        Service.printArray(arr131);


    }

    public void Task14(int[]arr) {
        Service.printArray(arr);
        int max14 = arr[0];
        int min14 = arr[1];
        for (int i=0; i<arr.length; i++){
            if (i%2==0){
                try {
                    if (arr[i]<arr[(i+2)]){max14= arr[(i+2)];}
                }catch (Exception e14) {System.out.println();}
            }else {
                try {
                    if (arr[i]>arr[(i+2)]){min14= arr[(i+2)];}
                }catch (Exception e141){ System.out.println(); }
            }

        }
        System.out.println(max14+min14);

    }

    public void Task15(int[]arr, int M) {
        Service.printArray(arr);
        int Result =0;
        for (int k: arr){
            if (k>M){
                if (Result==0){
                    Result=k;
                }else Result*=k;
            }
        }
        if (Result==0){
            System.out.println();
            System.out.println("Чисел >"+M+" нет");
        }else {
            System.out.println();
            System.out.println("M = " +M+ " = "+Result);
        }


    }

    public void Task16(int[]arr) {
        Service.printArray(arr);
        int max16=0;
        for (int k:arr){if (k>max16) max16=k;}
        for (int i=0; i<arr.length; i++){
            int temp=Service.modul(arr[i]);
            if (temp>max16) arr[i]=0;
        }
        System.out.println();
        System.out.println("max = "+max16);
        Service.printArray(arr);
        System.out.println();


    }

    public void Task17(int[]arr) {
        Service.printArray(arr);
        int P1 = 1;
        int P2 = 1;
        for (int k:arr){
            if (k<0){
                P1*=k;
            }else {P2*=k;}
        }
        System.out.println();
        System.out.println("P1 = "+P1);
        System.out.println("P2 = "+P2);
        System.out.println();
        if (P2>Service.modul(P1)) System.out.println("P2>P1");
        else System.out.println("P1>P2");
        System.out.println();


    }

    public void Task18(int[]arr) {
        Service.printArray(arr);
        int max18=0;
        for (int k:arr){ if (k>max18) max18=k;}
        for (int i=0; i<arr.length; i++){
            if (arr[i]==max18){arr[i]=0; break;}
        }
        System.out.println();
        Service.printArray(arr);
        System.out.println();

    }

    public void Task19(int K,int[]arr) {
        Service.printArray(arr);
        int count=0;
        for (int k:arr){
            if (k%10==K) count++;
        }
        int [] arr1 = new int[count];
        count=0;
        for (int i=0; i<arr.length; i++){
            if (arr[i]%10==K){
                arr1[count]=arr[i];
                count++;
            }
        }
        System.out.println();
        Service.printArray(arr1);
    }

}