package level1;

public class Level1_1 {
    public static void main(String[] args) {
        System.out.println("task 1");
        Tasks task1 = new Tasks();
        task1.Task1(25, 45);
        System.out.println();

        System.out.println("task 2");
        Tasks task2 = new Tasks();
        task2.Task2(new int[]{13, 10, 15, 16, 23, 10}, 5);
        System.out.println();

        System.out.println("task 3");
        Tasks task3 = new Tasks();
        task3.Task3(new int[]{10, 0, 25, 35, 0, 72, 0, 100});
        System.out.println();

        System.out.println("task 4");
        Tasks task4 = new Tasks();
        task4.Task4(new int[]{0, -10, 25, 35, 0, 72, 0, 100});
        System.out.println();

        System.out.println("task 5");
        Tasks task5 = new Tasks();
        task5.Task5(new int[]{0, 10, 25, 35, 65, 72, 65, 80});
        System.out.println();

        System.out.println("task 6");
        Tasks task6 = new Tasks();
        task6.Task6(new int[]{10, 13, 25, 35, 25, 72, 35, 100});
        System.out.println();
        System.out.println();

        System.out.println("task 7");
        Tasks task7 = new Tasks();
        task7.Task7(48, new int[]{10, 13, 25, 35, 25, 72, 35, 100});
        System.out.println();
        System.out.println();

        System.out.println("task 8");
        Tasks task8 = new Tasks();
        task8.Task8(new int[]{10, 0, -25, 35, -25, 72, 0, 100});
        System.out.println();
        System.out.println();


        System.out.println("task 9");
        Tasks task9 = new Tasks();
        task9.Task9(new int[]{10, 1, 25, 35, -25, 72, 0, 100});
        System.out.println();
        System.out.println();

        System.out.println("task 10");
        Tasks task10 = new Tasks();
        task10.Task10(new int[]{5, 3, 7, 1, -25, 72, 0, 100});
        System.out.println();
        System.out.println();

        System.out.println("task 11");
        Tasks task11 = new Tasks();
        task11.Task11(new int[]{4, 3, 6, 1, 25, 72, 0, 100}, 2, 1);
        System.out.println();
        System.out.println();


        System.out.println("task 12");
        Tasks task12 = new Tasks();
        task12.Task12(new int[]{4, 3, 6, 1, 25, 72, 0});
        System.out.println();
        System.out.println();


        System.out.println("task 13");
        Tasks task13 = new Tasks();
        task13.Task13(new int[]{4, 3, 6, 1, 0, 72, 25, 100});
        System.out.println();
        System.out.println();

        System.out.println("task 14");
        Tasks task14 = new Tasks();
        task14.Task14(new int[]{4, 3, 6, 1, 25, 72, 0, 100, 35});
        System.out.println();
        System.out.println();

        System.out.println("task 15");
        Tasks task15 = new Tasks();
        task15.Task15(new int[]{4, 3, 6, 1, 25, 72, 0, 100, 35}, 70);
        System.out.println();
        System.out.println();

        System.out.println("task 16");
        Tasks task16 = new Tasks();
        task16.Task16(new int[]{4, -3, 6, -80, -25, 72, -35, -100, 35});
        System.out.println();
        System.out.println();

        System.out.println("task 17");
        Tasks task17 = new Tasks();
        task17.Task17(new int[]{4, -3, 6, -80, -25, 72, -35, -100, 35});
        System.out.println();
        System.out.println();

        System.out.println("task 18");
        Tasks task18 = new Tasks();
        task18.Task18(new int[]{4, 3, 0, 100, 25, 0, 35, 100, 35});
        System.out.println();
        System.out.println();

        System.out.println("task 19");
        Tasks task19 = new Tasks();
        task19.Task19(6, new int[]{6, 3, 16, 100, 26, 0, 35, 106, 35});
        System.out.println();
        System.out.println();

    }
}
