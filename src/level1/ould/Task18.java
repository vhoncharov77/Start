package level1.ould;

import level1.Service;

public class Task18 {
    int [] arr;

    public Task18(int[] arr) {
        this.arr = arr;
    }

    public void firstMax() {
        Service.printArray(arr);
        int max18=0;
        for (int k:arr){ if (k>max18) max18=k;}
        for (int i=0; i<arr.length; i++){
            if (arr[i]==max18){arr[i]=0; break;}
        }
        System.out.println();
        Service.printArray(arr);
        System.out.println();

    }

}
