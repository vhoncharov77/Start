package level1.ould;

import level1.Service;

public class Task6 {
    int[] arr6;

    public Task6(int[] arr6) {
        this.arr6 = arr6;
    }

    public void newArrEven() {
        int ar6 =0;
        for (int i=0; i<arr6.length; i++){
            if((arr6[i]%2)==0){
                ar6++;
            }
        }
        int[] arr61 =new int[ar6];
        int n1=0;
        for (int i=0; i<arr6.length; i++){
            if(arr6[i]%2==0){
                arr61[n1]=arr6[i];
                n1++;
            }
        }
        Service.printArray(arr6);
        System.out.println();
        Service.printArray(arr61);

    }


}
