package level1.ould;

import level1.Service;

public class Task12 {
    int[] arr12;

    public Task12(int[] arr12) {
        this.arr12 = arr12;
    }
    public void swapNeighbors() {
        Service.printArray(arr12);
        try {
            int temp;
            for(int i=0; i<arr12.length; i+=2){
                temp=arr12[i]; arr12[i]=arr12[(i+1)]; arr12[(i+1)]=temp;
                //i++;
            }
        }catch (Exception oor){
            System.out.println();
            System.out.println("Не чётная длина массива!");
        }
        Service.printArray(arr12);
    }
}
