package level1.ould;

import level1.Service;

public class Task8 {
    int[] arr8;

    public Task8(int[] arr8) {
        this.arr8 = arr8;
    }
    public void maxMinZero() {
        Service.printArray(arr8);
        int count0 =0;
        int minus = 0;
        int plus = 0;
        for (int k:arr8){
            if (k<0) minus++;
            else if(k>0)plus++;
            else count0++;
        }
        System.out.println();
        System.out.println("Положительных - "+plus);
        System.out.println("Отрицательных - "+minus);
        System.out.println("Нулевых - "+count0);


    }
}
