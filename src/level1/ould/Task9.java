package level1.ould;

import level1.Service;

public class Task9 {
    int[] arr9;

    public Task9(int[] arr9) {
        this.arr9 = arr9;
    }
    public void maxMinChange() {
        int[] arr9t= new int[5];
        //arr9t[0] максимальное значение
        //arr9t[1] номер ячейки максимального значения
        //arr9t[2] минимальное значение
        //arr9t[3] номер ячейки минимального значения
        for (int i=0; i<arr9.length; i++){
            if(arr9[i]>arr9t[0]) {
                arr9t[0]=arr9[i];
                arr9t[1]=i;
            }else{
                if(arr9[i]<arr9t[2]) {
                    arr9t[2]=arr9[i];
                    arr9t[3]=i;
                }
            }
        }
        Service.printArray(arr9);
        arr9[arr9t[1]]=arr9t[2];
        arr9[arr9t[3]]=arr9t[0];
        System.out.println();
        Service.printArray(arr9);

    }

}
