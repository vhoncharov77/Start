package level1.ould;

import level1.Service;

public class Task17 {
    int [] arr;

    public Task17(int[] arr) {
        this.arr = arr;
    }

    public void max2VsMin1Modul() {
        Service.printArray(arr);
        int P1 = 1;
        int P2 = 1;
        for (int k:arr){
            if (k<0){
                P1*=k;
            }else {P2*=k;}
        }
        System.out.println();
        System.out.println("P1 = "+P1);
        System.out.println("P2 = "+P2);
        System.out.println();
        if (P2>Service.modul(P1)) System.out.println("P2>P1");
        else System.out.println("P1>P2");
        System.out.println();


    }
}
