package level1.ould;

import level1.Service;

public class Task11 {
    int[] arr11;
    int M;
    int L;

    public Task11(int[] arr11, int m, int l) {
        this.arr11 = arr11;
        M = m;
        L = l;
    }
    public void printArrML() {
        Service.printArray(arr11);
        System.out.println();
        System.out.println("M = "+M);
        System.out.println("L = "+L);
        for (int k:arr11) {
            if(k%M==L)
                System.out.print(k + " ");
        }


    }
}
