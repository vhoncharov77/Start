package level1.ould;

import level1.Service;

public class Task16 {
    int [] arr16;

    public Task16(int[] arr16) {
        this.arr16 = arr16;
    }

    public void zeroMaxI() {
        Service.printArray(arr16);
        int max16=0;
        for (int k:arr16){if (k>max16) max16=k;}
        for (int i=0; i<arr16.length; i++){
            int temp=Service.modul(arr16[i]);
            if (temp>max16) arr16[i]=0;
        }
        System.out.println();
        System.out.println("max = "+max16);
        Service.printArray(arr16);
        System.out.println();


    }
}
