package level1.ould;

import level1.Service;

public class Task19 {
    int [] arr;
    int K;

    public Task19(int[] arr, int k) {
        this.arr = arr;
        K = k;
    }

    public void newArrlastK() {
        Service.printArray(arr);
        int count=0;
        for (int k:arr){
            if (k%10==K) count++;
        }
        int [] arr1 = new int[count];
        count=0;
        for (int i=0; i<arr.length; i++){
            if (arr[i]%10==K){
                arr1[count]=arr[i];
                count++;
            }
        }
        System.out.println();
        Service.printArray(arr1);
    }
}
