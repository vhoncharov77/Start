package zadachi;

public class Zadachi1 {
    public static void main(String[] args) {
        //пример 1
        System.out.println("пример1");
        int a =75;
        int b =25;
        System.out.println(((a-b)>0)?a:b);
        System.out.println("----------------------");

        //пример 2
        System.out.println("пример2");
        int e =5;
        int sum =0;
        int [] mass1={13, 10, 15, 16, 23, 10};
        for(int i=0; i< mass1.length;i++){
            if (mass1[i]%e==0){
             sum=mass1[i]+sum;
            }
            }
        System.out.println(sum);
        System.out.println("----------------------");

        //пример 3
        System.out.println("пример3");
        int ar2 =0;
        int [] arr1= {10, 0, 25, 35, 0, 72, 0, 100};
        for (int i=0; i<arr1.length; i++){
            if(arr1[i]==0){
                ar2++;
            }
        }
        int[] arr2 =new int[ar2];
        int n=0;
        for (int i=0; i<arr1.length; i++){
            if(arr1[i]==0){
                arr2[n]=i;
                n++;
            }
        }
        printArray(arr1);
        System.out.println();
        printArray(arr2);
        System.out.println();
        System.out.println("----------------------");

        //пример 4
        System.out.println();
        System.out.println("пример4");
        int [] arr3= {0, -10, 25, 35, 0, 72, 0, 100};
        for (int i=0; i<arr3.length; i++){
            if(arr3[i]<0){
                System.out.println("Первое отрицательное"); break;
            }else{ if(arr3[i]>0){System.out.println("Первое положительное"); break;
                }
            }
        }
        System.out.println("----------------------");


        //пример 5
        System.out.println();
        System.out.println("пример5");
        int [] arr4= {0, 10, 25, 35, 65, 72, 65, 80};
        String out = "Отсортирован";
        for (int i=0; i<(arr4.length-1); i++){
            if(arr4[i]>arr4[i+1]) {
                out = "Не отсортирован";
                break;
            }
        }
        System.out.println(out);
        System.out.println("----------------------");

        //пример 6
        System.out.println();
        System.out.println("пример6");
        int ar5 =0;
        int [] arr5= {10, 13, 25, 35, 25, 72, 35, 100};
        for (int i=0; i<arr5.length; i++){
            if((arr5[i]%2)==0){
                ar5++;
            }
        }
        int[] arr6 =new int[ar5];
        int n1=0;
        for (int i=0; i<arr1.length; i++){
            if(arr5[i]%2==0){
                arr6[n1]=arr5[i];
                n1++;
            }
        }
        printArray(arr5);
        System.out.println();
        printArray(arr6);

        System.out.println();
        System.out.println("----------------------");

        //пример 7
        System.out.println();
        System.out.println("пример7");
        int z =14;
        int count =0;
        int [] arr7= {10, 13, 25, 35, 25, 72, 35, 100};
        printArray(arr7);
        for (int i=0; i<arr7.length; i++){
            if(arr7[i]>z){
                arr7[i]=z;
                count++;
            }
        }
        System.out.println();
        printArray(arr7);
        System.out.println();
        System.out.println("Количество замен = "+count);
        System.out.println("----------------------");

        //пример 8
        System.out.println();
        System.out.println("пример8");
        int [] arr8= {10, 0, -25, 35, -25, 72, 0, 100};
        printArray(arr8);
        int count0 =0;
        int minus = 0;
        int plus = 0;
        for (int k:arr8){
            if (k<0) minus++;
            else if(k>0)plus++;
            else count0++;
        }
        System.out.println();
        System.out.println("Положительных - "+plus);
        System.out.println("Отрицательных - "+minus);
        System.out.println("Нулевых - "+count0);
        System.out.println("----------------------");

        //пример 9
        System.out.println();
        System.out.println("пример9");
        int [] arr9= {10, 1, 25, 35, -25, 72, 0, 100};
        int[] arr9t= new int[5];
        //arr9t[0] максимальное значение
        //arr9t[1] номер ячейки максимального значения
        //arr9t[2] минимальное значение
        //arr9t[3] номер ячейки минимального значения
        for (int i=0; i<arr9.length; i++){
            if(arr9[i]>arr9t[0]) {
                arr9t[0]=arr9[i];
                arr9t[1]=i;
            }else{
                if(arr9[i]<arr9t[2]) {
                    arr9t[2]=arr9[i];
                    arr9t[3]=i;
            }
        }
        }
        printArray(arr9);
        arr9[arr9t[1]]=arr9t[2];
        arr9[arr9t[3]]=arr9t[0];
        System.out.println();
        printArray(arr9);

        System.out.println();
        System.out.println("----------------------");


        //пример 10
        System.out.println();
        System.out.println("пример10");
        int [] arr10= {5, 3, 7, 1, -25, 72, 0, 100};
        printArray(arr10);
        System.out.println();
        for (int i=0; i<arr10.length;i++) {
            if(arr10[i]<i)
            System.out.print(arr10[i] + " ");
        }
        System.out.println("----------------------");

        //пример 11
        System.out.println();
        System.out.println("пример11");
        int [] arr11= {4, 3, 6, 1, 25, 72, 0, 100};
        printArray(arr11);
        System.out.println();
        int M=2;
        int L=1;
        System.out.println();
        for (int k:arr11) {
            if(k%M==L)
                System.out.print(k + " ");
        }
        System.out.println("----------------------");

        //пример 12
        System.out.println();
        System.out.println("пример12");
        int [] arr12= {4, 3, 6, 1, 25, 72, 0, 10};
        printArray(arr12);
        try {
            int temp;
            for(int i=0; i<arr12.length; i+=2){
                temp=arr12[i]; arr12[i]=arr12[(i+1)]; arr12[(i+1)]=temp;
                //i++;
            }
        }catch (Exception oor){
            System.out.println();
            System.out.println("Не чётная длина массива!");
        }
        printArray(arr12);

        System.out.println();
        System.out.println("----------------------");

        //пример 13
        System.out.println();
        System.out.println("пример13");
        int [] arr13= {4, 3, 6, 1, 25, 72, 0, 100};
        printArray(arr13);
        int count13=0;
        for(int i=0; i<arr13.length; i++){
            if (arr13[i]==0){
                count13 = i+1; break;
            }
        }
        int [] arr131 = new int[count13];
        for (int i=0; i<arr131.length; i++){
            arr131[i]=arr13[i];
        }
        System.out.println();
        printArray(arr131);

        System.out.println();
        System.out.println("----------------------");

        //пример 14
        System.out.println();
        System.out.println("пример14");
        int [] arr14= {4, 3, 6, 1, 25, 72, 0, 100, 35};
        printArray(arr14);
        int max14 = arr14[0];
        int min14 = arr14[1];
        for (int i=0; i<arr14.length; i++){
            if (i%2==0){
                try {
                    if (arr14[i]<arr14[(i+2)]){max14= arr14[(i+2)];}
                }catch (Exception e14) {System.out.println();}
            }else {
                try {
                    if (arr14[i]>arr14[(i+2)]){min14= arr14[(i+2)];}
                }catch (Exception e141){ System.out.println(); }
            }

        }
        System.out.println(max14+min14);
        System.out.println("----------------------");

        //пример 15
        System.out.println();
        System.out.println("пример15");
        int [] arr15= {4, 3, 6, 1, 25, 72, 0, 100, 35};
        printArray(arr15);
        int M15=70;
        int Result =0;
        for (int k: arr15){
            if (k>M15){
                if (Result==0){
                    Result=k;
                }else Result*=k;
            }
        }
        if (Result==0){
            System.out.println();
            System.out.println("Чисел >"+M15+" нет");
        }else {
            System.out.println();
            System.out.println("M = " +M15+ " = "+Result);
        }
        System.out.println("----------------------");


        //пример 16
        System.out.println();
        System.out.println("пример16");
        int max16=0;
        int [] arr16= {4, -3, 6, -80, -25, 72, -35, -100, 35};
        printArray(arr16);
        for (int k:arr16){if (k>max16) max16=k;}
        for (int i=0; i<arr16.length; i++){
            int temp=modul(arr16[i]);
            if (temp>max16) arr16[i]=0;
        }
        System.out.println();
        System.out.println("max = "+max16);
        printArray(arr16);

        System.out.println();
        System.out.println("----------------------");


        //пример 17
        System.out.println();
        System.out.println("пример17");
        int [] arr17= {4, -3, 6, -80, -25, 72, -35, -100, 35};
        printArray(arr17);
        int P1 = 1;
        int P2 = 1;
        for (int k:arr17){
            if (k<0){
                P1*=k;
            }else {P2*=k;}
        }
        System.out.println();
        System.out.println("P1 = "+P1);
        System.out.println("P2 = "+P2);
        System.out.println();
        if (P2>modul(P1)) System.out.println("P2>P1");
        else System.out.println("P1>P2");
        System.out.println("----------------------");

        //пример 18
        System.out.println();
        System.out.println("пример18");
        int [] arr18= {4, 3, 0, 100, 25, 0, 35, 100, 35};
        printArray(arr18);
        int max18=0;
        for (int k:arr18){ if (k>max18) max18=k;}
        for (int i=0; i<arr18.length; i++){
            if (arr18[i]==max18){arr18[i]=0; break;}
        }
        System.out.println();
        printArray(arr18);

        System.out.println();
        System.out.println("----------------------");

        //пример 19
        System.out.println();
        System.out.println("пример19");
        int [] arr19= {6, 3, 16, 100, 26, 0, 35, 106, 35};
        printArray(arr19);
        int count19=0;
        int k19=6;
        for (int k:arr19){
            if (k%10==k19) count19++;
        }
        int [] arr191 = new int[count19];
        count19=0;
        for (int i=0; i<arr19.length; i++){
            if (arr19[i]%10==k19){
                arr191[count19]=arr19[i];
                count19++;
            }
        }
        System.out.println();
        printArray(arr191);
        System.out.println();
        System.out.println("---------------------------------");
        System.out.println("По моему всё!!!");

    }





    static void printArray(int[] a) {
        System.out.println("Все значения в массиве");
        for (int l : a) {
            System.out.print(l + " ");
        }
    }
    public static int modul (int a)
    {
        if (a<0) a=a*-1;
        return(a);
    }

    }


