package vga.com;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int leng=0;
        leng=sc.nextInt();
        int[] a = new int[leng];
        Ars.inputArray(a);
        Ars.printArray(a);

        int k=0;
        //Сортировка массива
        while (k<(a.length-1)) {
            int i=0;
            while (i < (a.length - 1)) {
                if (a[i] > a[i + 1]) {
                    int t = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = t;
                    i++;
                } else i++;
            }
            k++;
        }
        System.out.println();
        System.out.println("После сортировки");
        Ars.printArray(a);

    }


}