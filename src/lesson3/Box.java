package lesson3;

public abstract class Box {
    public abstract int mass(int density);
    public abstract void print();

}
