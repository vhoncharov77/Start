package lesson3;

import java.util.Set;

public class Stone extends Box {
    private int length;
    private int width;
    private int height;

    Stone(int length, int width, int height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    @Override
    public int mass(int density) {
        int mas = 0;
        mas = length * width * height * density;
        return mas;
    }
    @Override
    public void print() {
        System.out.println("длина - " + length);
        System.out.println("ширина - " + width);
        System.out.println("высота - " + height);
    }

    @Override
    public String toString() {
        return "Камень: длина - " + length + "; ширина - " + width + "; Высота - " + height;

    }
}