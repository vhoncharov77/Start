package lesson3;

public  class Cat {
    private String name;
    private int age;
    private int weight;
    Cat(String name, int age, int weight){
        this.name=name;
        this.age=age;
        this.weight=weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }
    public void print(){
        System.out.println("Name - "+ name);
        System.out.println("age - "+ age+" years");
        System.out.println("weight - "+ weight+" kg.");
    }
    public void plump(int b){//потолстеть
        weight+=b;
    }
    public void loseWeight(int l){
        weight-=l;
    }

    @Override
    public String toString() {
        return "Кот: Имя - "+name+"; Возраст - "+age+"; Вес - "+weight;
    }
}
